﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Programacion.Startup))]
namespace Programacion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
